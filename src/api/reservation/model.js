import mongoose, {Schema} from 'mongoose'
import {isEmpty} from 'lodash'
import {endOfDay, startOfDay} from 'date-fns'
import {ERROR} from '../../commons/constants'

const reservationSchema = new Schema({
  departureDate: {
    required: true,
    type: Date,
    default: Date.now
  },
  returnDate: {
    type: Date
  },
  originCity: {
    type: String,
    required: true,
    lowercase: true,
    trim: true
  },
  destinationCity: {
    type: String,
    required: true,
    lowercase: true,
    trim: true
  },
  service: {
    required: true,
    type: String,
    enum: [
      'EXECUTIVE', 'ECONOMIC'
    ],
    default: 'ECONOMIC'
  },
  adults: {
    type: Number,
    default: 0,
    min: 0
  },
  children: {
    type: Number,
    default: 0,
    min: 0
  },
  babies: {
    type: Number,
    default: 0,
    min: 0
  },
  identificationCard: {
    required: true,
    type: Number,
    min: 0
  },
  age: {
    required: true,
    type: Number,
    min: 18
  },
  cost: {
    type: Number,
    required: true,
    min: 0
  },
  flight: {
    type: Schema.ObjectId,
    ref: 'Flight',
    required: true
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => {
      delete ret._id
    }
  }
})

reservationSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      departureDate: this.departureDate,
      returnDate: this.returnDate,
      originCity: this.originCity,
      destinationCity: this.destinationCity,
      service: this.service,
      adults: this.adults,
      children: this.children,
      babies: this.babies,
      identificationCard: this.identificationCard,
      age: this.age,
      flight: this.flight,
      cost: this.cost,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full
      ? {
        ...view
        // add properties for a full view
      }
      : view
  }
}

reservationSchema.pre('validate', async function (next, data) {
  const reservation = this
  const endDayRegister = endOfDay(this.departureDate)
  const startDayRegister = startOfDay(this.departureDate)
  const found = await reservation.constructor.find({
    identificationCard: this.identificationCard,
    departureDate: {'$gte': startDayRegister, '$lte': endDayRegister}
  })
  if (isEmpty(found)) {
    next()
  } else {
    next(ERROR.RESERVATION.SAVE_RESERVATION.code)
  }
})
const model = mongoose.model('Reservation', reservationSchema)

export const schema = model.schema
export default model
