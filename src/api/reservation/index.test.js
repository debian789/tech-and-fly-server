import request from 'supertest'
import {apiRoot} from '../../config'
import express from '../../services/express'
import {ERROR} from '../../commons/constants'
import routes from '.'
import Reservation from './model'
import Flight from '../flight/model'

const app = () => express(apiRoot, routes)

let reservation
let flight
const identification = 1234343212

beforeEach(async () => {
  const dateTemp = new Date()
  flight = await Flight.create({
    code: 'test',
    cost: 23,
    originCity: 'test',
    destinationCity: 'test',
    status: 'DELAYED',
    adults: 2,
    children: 2,
    babies: 0,
    checkOutTime: new Date(),
    timeOfArrival: new Date(),
    serviceEconomic: 12,
    serviceEjecutive: 23
  })

  reservation = await Reservation.create({
    departureDate: dateTemp,
    returnDate: dateTemp,
    originCity: 'test',
    destinationCity: 'test',
    service: 'ECONOMIC',
    adults: 1,
    children: 0,
    babies: 0,
    identificationCard: identification,
    age: 18,
    cost: 23,
    flight: flight.id
  })
})

test('POST /reservations 200', async () => {
  const dateTemp = new Date()
  const {status, body} = await request(app())
    .post(`${apiRoot}`)
    .send({
      departureDate: dateTemp,
      returnDate: dateTemp,
      originCity: 'test',
      destinationCity: 'test',
      service: 'ECONOMIC',
      adults: 8,
      children: 3,
      babies: 0,
      identificationCard: identification,
      age: 19,
      cost: 23,
      flight: flight.id
    })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.code).toEqual(ERROR.RESERVATION.SAVE_RESERVATION.code)
  expect(body.message).toEqual(ERROR.RESERVATION.SAVE_RESERVATION.message)
})

test('POST /reservations 201', async () => {
  const dateTemp = new Date(2222, 11, 12)
  const identificationTemp = 99999666661111
  const {status, body} = await request(app())
    .post(`${apiRoot}`)
    .send({
      departureDate: dateTemp,
      returnDate: dateTemp,
      originCity: 'test',
      destinationCity: 'test',
      service: 'ECONOMIC',
      adults: 8,
      children: 3,
      babies: 0,
      identificationCard: identificationTemp,
      age: 19,
      cost: 23,
      flight: flight.id
    })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.departureDate).toEqual(dateTemp.toISOString())
  expect(body.returnDate).toEqual(dateTemp.toISOString())
  expect(body.originCity).toEqual('test')
  expect(body.destinationCity).toEqual('test')
  expect(body.service).toEqual('ECONOMIC')
  expect(body.adults).toEqual(8)
  expect(body.children).toEqual(3)
  expect(body.babies).toEqual(0)
  expect(body.identificationCard).toEqual(identificationTemp)
  expect(body.age).toEqual(19)
  expect(body.cost).toEqual(23)
  expect(body.flight).toEqual(flight.id)
})

test('GET /reservations 200', async () => {
  const {status, body} = await request(app()).get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /reservations/:identification 200', async () => {
  const {status, body} = await request(app()).get(`${apiRoot}/${reservation.identificationCard}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body[0].id).toEqual(reservation.id)
})

test('GET /reservations/:identification 200 empty', async () => {
  const {body} = await request(app()).get(`${apiRoot}/${identification}12345678`)
  expect(typeof body).toEqual('object')
  expect(body).toEqual([])
})

test('PUT /reservations/:id 200', async () => {
  const tempDate = new Date()
  const {status, body} = await request(app())
    .put(`${apiRoot}/${reservation.id}`)
    .send({
      departureDate: tempDate,
      returnDate: tempDate,
      originCity: 'test',
      destinationCity: 'test',
      service: 'ECONOMIC',
      adults: 2,
      children: 0,
      babies: 1,
      identificationCard: 3,
      age: 20,
      cost: 23,
      flight: flight.id
    })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(reservation.id)
  expect(body.departureDate).toEqual(tempDate.toISOString())
  expect(body.returnDate).toEqual(tempDate.toISOString())
  expect(body.originCity).toEqual('test')
  expect(body.destinationCity).toEqual('test')
  expect(body.service).toEqual('ECONOMIC')
  expect(body.adults).toEqual(2)
  expect(body.children).toEqual(0)
  expect(body.babies).toEqual(1)
  expect(body.identificationCard).toEqual(3)
  expect(body.age).toEqual(20)
  expect(body.cost).toEqual(23)
  expect(body.flight).toEqual(flight.id)
})

test('PUT /reservations/:id 404', async () => {
  const tempDate = new Date()
  const {status} = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({
      departureDate: tempDate,
      returnDate: tempDate,
      originCity: 'test',
      destinationCity: 'test',
      service: 'ECONOMIC',
      adults: 34,
      children: 1,
      babies: 2,
      identificationCard: 3,
      age: 23,
      cost: 34,
      flight: flight.id
    })
  expect(status).toBe(404)
})

test('DELETE /reservations/:id 204', async () => {
  const {status} = await request(app()).delete(`${apiRoot}/${reservation.id}`)
  expect(status).toBe(204)
})

test('DELETE /reservations/:id 404', async () => {
  const {status} = await request(app()).delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
