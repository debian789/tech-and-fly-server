import {Router} from 'express'
import {middleware as query} from 'querymen'
import {middleware as body} from 'bodymen'
import {create, index, update, destroy, showByIdentificationCard} from './controller'
import {schema} from './model'

const router = new Router()
const {
  departureDate,
  originCity,
  destinationCity,
  returnDate,
  service,
  adults,
  children,
  babies,
  identificationCard,
  age,
  cost,
  flight
} = schema.tree

/**
 * @api {post} /reservations Create reservation
 * @apiName CreateReservation
 * @apiGroup Reservation
 * @apiParam departureDate Reservation's departureDate.
 * @apiParam returnDate Reservation's returnDate.
 * @apiParam originCity Reservation's originCity.
 * @apiParam destinationCity Reservation's destinationCity.
 * @apiParam service Reservation's service.
 * @apiParam adults Reservation's adults.
 * @apiParam children Reservation's children.
 * @apiParam babies Reservation's babies.
 * @apiParam identificationCard Reservation's identificationCard.
 * @apiParam age Reservation's age.
 * @apiSuccess {Object} reservation Reservation's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Reservation not found.
 */
router.post('/', body({
  departureDate,
  originCity,
  destinationCity,
  returnDate,
  service,
  adults,
  children,
  babies,
  identificationCard,
  age,
  cost,
  flight
}), create)

/**
 * @api {get} /reservations Retrieve reservations
 * @apiName RetrieveReservations
 * @apiGroup Reservation
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of reservations.
 * @apiSuccess {Object[]} rows List of reservations.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/', query(), index)

/**
 * @api {get} /reservations/:identification Retrieve reservation
 * @apiName RetrieveReservation
 * @apiGroup Reservation
 * @apiSuccess {Object} reservation Reservation's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Reservation not found.
 */
router.get('/:identification', showByIdentificationCard)

/**
 * @api {put} /reservations/:id Update reservation
 * @apiName UpdateReservation
 * @apiGroup Reservation
 * @apiParam departureDate Reservation's departureDate.
 * @apiParam returnDate Reservation's returnDate.
 * @apiParam originCity Reservation's originCity.
 * @apiParam destinationCity Reservation's destinationCity.
 * @apiParam service Reservation's service.
 * @apiParam adults Reservation's adults.
 * @apiParam children Reservation's children.
 * @apiParam babies Reservation's babies.
 * @apiParam identificationCard Reservation's identificationCard.
 * @apiParam age Reservation's age.
 * @apiSuccess {Object} reservation Reservation's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Reservation not found.
 */
router.put('/:id', body({
  departureDate,
  originCity,
  destinationCity,
  returnDate,
  service,
  adults,
  children,
  babies,
  identificationCard,
  age,
  cost,
  flight
}), update)

/**
 * @api {delete} /reservations/:id Delete reservation
 * @apiName DeleteReservation
 * @apiGroup Reservation
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Reservation not found.
 */
router.delete('/:id', destroy)

export default router
