import { success, notFound } from '../../services/response/'
import Reservation from './model'
import {ERROR} from '../../commons/constants'

export const create = ({ bodymen: { body } }, res, next) =>
  Reservation.create(body)
    .then((reservation) => reservation.view(true))
    .then(success(res, 201))
    .catch((err) => {
      if (err && err === 1) {
        next(res.status(200).json(ERROR.RESERVATION.SAVE_RESERVATION))
      } else {
        next(err)
      }
    })

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Reservation.count(query)
    .then(count => Reservation.find(query, select, cursor)
      .then((reservations) => ({
        count,
        rows: reservations.map((reservation) => reservation.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const showByIdentificationCard = ({ params }, res, next) =>
  Reservation.find({identificationCard: params.identification})
    .then(notFound(res))
    .then(reservation => reservation)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Reservation.findById(params.id)
    .then(notFound(res))
    .then((reservation) => reservation ? Object.assign(reservation, body).save() : null)
    .then((reservation) => reservation ? reservation.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Reservation.findById(params.id)
    .then(notFound(res))
    .then((reservation) => reservation ? reservation.remove() : null)
    .then(success(res, 204))
    .catch(next)
