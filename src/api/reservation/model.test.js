import Reservation from './model'
import Flight from '../flight/model'

let reservation
let flight

beforeEach(async () => {
  const dateTemp = new Date()
  flight = await Flight.create({
    code: 'test',
    cost: 23,
    originCity: 'test',
    destinationCity: 'test',
    status: 'DELAYED',
    adults: 2,
    children: 2,
    babies: 0,
    checkOutTime: new Date(),
    timeOfArrival: new Date(),
    serviceEconomic: 12,
    serviceEjecutive: 23
  })
  reservation = await Reservation.create({
    departureDate: dateTemp,
    returnDate: dateTemp,
    originCity: 'test',
    destinationCity: 'test',
    service: 'ECONOMIC',
    adults: 2,
    children: 0,
    babies: 3,
    identificationCard: 234,
    age: 23,
    cost: 23,
    flight: flight.id
  })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = reservation.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(reservation.id)
    expect(view.departureDate).toBe(reservation.departureDate)
    expect(view.returnDate).toBe(reservation.returnDate)
    expect(view.originCity).toBe(reservation.originCity)
    expect(view.destinationCity).toBe(reservation.destinationCity)
    expect(view.service).toBe(reservation.service)
    expect(view.adults).toBe(reservation.adults)
    expect(view.children).toBe(reservation.children)
    expect(view.babies).toBe(reservation.babies)
    expect(view.identificationCard).toBe(reservation.identificationCard)
    expect(view.age).toBe(reservation.age)
    expect(view.cost).toBe(reservation.cost)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = reservation.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(reservation.id)
    expect(view.departureDate).toBe(reservation.departureDate)
    expect(view.returnDate).toBe(reservation.returnDate)
    expect(view.originCity).toBe(reservation.originCity)
    expect(view.destinationCity).toBe(reservation.destinationCity)
    expect(view.service).toBe(reservation.service)
    expect(view.adults).toBe(reservation.adults)
    expect(view.children).toBe(reservation.children)
    expect(view.babies).toBe(reservation.babies)
    expect(view.identificationCard).toBe(reservation.identificationCard)
    expect(view.age).toBe(reservation.age)
    expect(view.cost).toBe(reservation.cost)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
