import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Flight } from '.'

const app = () => express(apiRoot, routes)

let flight

beforeEach(async () => {
  const registerDate = new Date()
  flight = await Flight.create({ code: 'test', cost: 23, originCity: 'test', destinationCity: 'test', status: 'DELAYED', checkOutTime: registerDate, timeOfArrival: registerDate, adults: 1, children: 0, babies: 0, serviceEconomic: 12, serviceEjecutive: 23 })
})

test('POST /flights 201', async () => {
  const registerDate = new Date()
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ code: 'test', cost: 23, originCity: 'test', destinationCity: 'test', status: 'DELAYED', checkOutTime: registerDate, timeOfArrival: registerDate, adults: 1, children: 0, babies: 0, serviceEconomic: 12, serviceEjecutive: 23 })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.code).toEqual('test')
  expect(body.cost).toEqual(23)
  expect(body.originCity).toEqual('test')
  expect(body.destinationCity).toEqual('test')
  expect(body.status).toEqual('DELAYED')
  expect(body.adults).toEqual(1)
  expect(body.children).toEqual(0)
  expect(body.babies).toEqual(0)
  expect(body.serviceEconomic).toEqual(12)
  expect(body.serviceEjecutive).toEqual(23)
  expect(body.checkOutTime).toEqual(registerDate.toISOString())
  expect(body.timeOfArrival).toEqual(registerDate.toISOString())
})

test('GET /flights 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /flights/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${flight.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(flight.id)
})

test('GET /flights/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /flights/:id 200', async () => {
  const registerDate = new Date()
  const { status, body } = await request(app())
    .put(`${apiRoot}/${flight.id}`)
    .send({ code: 'test', cost: 34, originCity: 'test', destinationCity: 'test', status: 'DELAYED', checkOutTime: registerDate, timeOfArrival: registerDate, adults: 2, children: 2, babies: 0, serviceEconomic: 12, serviceEjecutive: 23 })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(flight.id)
  expect(body.code).toEqual('test')
  expect(body.cost).toEqual(34)
  expect(body.originCity).toEqual('test')
  expect(body.destinationCity).toEqual('test')
  expect(body.status).toEqual('DELAYED')
  expect(body.adults).toEqual(2)
  expect(body.children).toEqual(2)
  expect(body.babies).toEqual(0)
  expect(body.serviceEconomic).toEqual(12)
  expect(body.serviceEjecutive).toEqual(23)
  expect(body.checkOutTime).toEqual(registerDate.toISOString())
  expect(body.timeOfArrival).toEqual(registerDate.toISOString())
})

test('PUT /flights/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ code: 'test', cost: 34, originCity: 'test', destinationCity: 'test', status: 'DELAYED', checkOutTime: new Date(), timeOfArrival: new Date(), adults: 2, children: 2, babies: 0, serviceEconomic: 12, serviceEjecutive: 23 })
  expect(status).toBe(404)
})

test('DELETE /flights/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${flight.id}`)
  expect(status).toBe(204)
})

test('DELETE /flights/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
