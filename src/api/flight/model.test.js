import {Flight} from '.'

let flight

beforeEach(async () => {
  flight = await Flight.create({
    code: 'test',
    cost: 34,
    originCity: 'test',
    destinationCity: 'test',
    status: 'DELAYED',
    checkOutTime: new Date(),
    timeOfArrival: new Date(),
    adults: 1,
    children: 0,
    babies: 0,
    serviceEconomic: 12,
    serviceEjecutive: 23
  })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = flight.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(flight.id)
    expect(view.code).toBe(flight.code)
    expect(view.cost).toBe(flight.cost)
    expect(view.originCity).toBe(flight.originCity)
    expect(view.destinationCity).toBe(flight.destinationCity)
    expect(view.status).toBe(flight.status)
    expect(view.checkOutTime).toBe(flight.checkOutTime)
    expect(view.timeOfArrival).toBe(flight.timeOfArrival)
    expect(view.adults).toBe(flight.adults)
    expect(view.children).toBe(flight.children)
    expect(view.babies).toBe(flight.babies)
    expect(view.serviceEconomic).toBe(flight.serviceEconomic)
    expect(view.serviceEjecutive).toBe(flight.serviceEjecutive)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = flight.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(flight.id)
    expect(view.code).toBe(flight.code)
    expect(view.cost).toBe(flight.cost)
    expect(view.originCity).toBe(flight.originCity)
    expect(view.destinationCity).toBe(flight.destinationCity)
    expect(view.status).toBe(flight.status)
    expect(view.adults).toBe(flight.adults)
    expect(view.children).toBe(flight.children)
    expect(view.babies).toBe(flight.babies)
    expect(view.serviceEconomic).toBe(flight.serviceEconomic)
    expect(view.serviceEjecutive).toBe(flight.serviceEjecutive)
    expect(view.checkOutTime).toBe(flight.checkOutTime)
    expect(view.timeOfArrival).toBe(flight.timeOfArrival)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
