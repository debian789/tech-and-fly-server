import { success, notFound } from '../../services/response/'
import { Flight } from '.'

export const create = ({ bodymen: { body } }, res, next) => {
  return Flight.create(body)
    .then((flight) => flight.view(true))
    .then(success(res, 201))
    .catch(next)
}

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Flight.count(query)
    .then(count => Flight.find(query, select, cursor)
      .then((flights) => ({
        count,
        rows: flights.map((flight) => flight.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Flight.findById(params.id)
    .then(notFound(res))
    .then((flight) => flight ? flight.view() : null)
    .then(success(res))
    .catch(next)

export const filterAvailableFlight = ({ bodymen: { body }, params }, res, next) =>
  Flight.find({
    originCity: { $regex: '.*' + body.originCity + '.*' },
    destinationCity: { $regex: '.*' + body.destinationCity + '.*' },
    checkOutTime: {'$gte': body.checkOutTime},
    timeOfArrival: {'$lte': body.timeOfArrival ? body.timeOfArrival : new Date(2999, 11, 11)},
    serviceEconomic: {'$gte': body.serviceEconomic},
    serviceEjecutive: {'$gte': body.serviceEjecutive},
    adults: {'$gte': body.adults},
    children: {'$gte': body.children},
    babies: {'$gte': body.babies}
  })
    .then(notFound(res))
    .then((flight) => flight)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Flight.findById(params.id)
    .then(notFound(res))
    .then((flight) => flight ? Object.assign(flight, body).save() : null)
    .then((flight) => flight ? flight.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Flight.findById(params.id)
    .then(notFound(res))
    .then((flight) => flight ? flight.remove() : null)
    .then(success(res, 204))
    .catch(next)
