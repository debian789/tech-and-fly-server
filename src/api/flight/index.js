import {Router} from 'express'
import {middleware as query} from 'querymen'
import {middleware as body} from 'bodymen'
import {
  create,
  index,
  show,
  update,
  destroy,
  filterAvailableFlight
} from './controller'
import {schema} from './model'
export Flight, {schema} from './model'

const router = new Router()
const {
  code,
  cost,
  originCity,
  destinationCity,
  status,
  checkOutTime,
  timeOfArrival,
  adults,
  children,
  babies,
  serviceEconomic,
  serviceEjecutive
} = schema.tree

/**
 * @api {post} /flights Create flight
 * @apiName CreateFlight
 * @apiGroup Flight
 * @apiParam code Flight's code.
 * @apiParam originCity Flight's originCity.
 * @apiParam destinationCity Flight's destinationCity.
 * @apiParam status Flight's status.
 * @apiParam checkOutTime Flight's checkOutTime.
 * @apiParam timeOfArrival Flight's timeOfArrival.
 * @apiSuccess {Object} flight Flight's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Flight not found.
 */
router.post('/', body({
  code,
  cost,
  originCity,
  destinationCity,
  status,
  checkOutTime,
  timeOfArrival,
  adults,
  children,
  babies,
  serviceEconomic,
  serviceEjecutive
}), create)

/**
 * @api {get} /flights Retrieve flights
 * @apiName RetrieveFlights
 * @apiGroup Flight
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of flights.
 * @apiSuccess {Object[]} rows List of flights.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/', query(), index)

/**
 * @api {get} /flights/:id Retrieve flight
 * @apiName RetrieveFlight
 * @apiGroup Flight
 * @apiSuccess {Object} flight Flight's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Flight not found.
 */
router.get('/:id', show)

router.post('/search', body({
  originCity: {required: true},
  destinationCity: {required: true},
  checkOutTime: {required: true},
  serviceEconomic: {type: Number, required: true},
  serviceEjecutive: { type: Number, required: true },
  adults: {type: Number, required: true},
  children: {type: Number, required: true},
  babies: {type: Number, required: true}
}), filterAvailableFlight)

/**
 * @api {put} /flights/:id Update flight
 * @apiName UpdateFlight
 * @apiGroup Flight
 * @apiParam code Flight's code.
 * @apiParam originCity Flight's originCity.
 * @apiParam destinationCity Flight's destinationCity.
 * @apiParam status Flight's status.
 * @apiParam checkOutTime Flight's checkOutTime.
 * @apiParam timeOfArrival Flight's timeOfArrival.
 * @apiSuccess {Object} flight Flight's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Flight not found.
 */
router.put('/:id', body({
  code,
  cost,
  originCity,
  destinationCity,
  status,
  checkOutTime,
  timeOfArrival,
  adults,
  children,
  babies,
  serviceEconomic,
  serviceEjecutive
}), update)

/**
 * @api {delete} /flights/:id Delete flight
 * @apiName DeleteFlight
 * @apiGroup Flight
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Flight not found.
 */
router.delete('/:id', destroy)

export default router
