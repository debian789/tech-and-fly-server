import mongoose, {Schema} from 'mongoose'
import {ConfigurationInstance, isCost} from '../../commons/utils'
import {PERCENTAGE_INCREMENT_SCHEDULE} from '../../commons/constants'

const flightSchema = new Schema({
  code: {
    type: String,
    required: true,
    lowercase: true,
    trim: true
  },
  originCity: {
    type: String,
    required: true,
    lowercase: true,
    trim: true
  },
  destinationCity: {
    type: String,
    required: true,
    lowercase: true,
    trim: true
  },
  cost: {
    type: Number,
    required: true,
    min: 0
  },
  status: {
    type: String,
    enum: [
      'DELAYED', 'ARRIVED', 'DEPARTED', 'CANCELED'
    ],
    default: 'DELAYED'
  },
  serviceEconomic: {
    type: Number,
    required: true
  },
  serviceEjecutive: {
    type: Number,
    required: true
  },
  checkOutTime: {
    type: Date,
    default: Date.now
  },
  timeOfArrival: {
    type: Date,
    default: Date.now
  },
  adults: {
    required: true,
    type: Number,
    min: 0
  },
  children: {
    required: true,
    type: Number,
    min: 0
  },
  babies: {
    required: true,
    type: Number,
    min: 0
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => {
      delete ret._id
    }
  }
})

flightSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      code: this.code,
      cost: this.cost,
      originCity: this.originCity,
      destinationCity: this.destinationCity,
      status: this.status,
      checkOutTime: this.checkOutTime,
      timeOfArrival: this.timeOfArrival,
      adults: this.adults,
      children: this.children,
      babies: this.babies,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      serviceEconomic: this.serviceEconomic,
      serviceEjecutive: this.serviceEjecutive
    }

    return full
      ? {
        ...view
        // add properties for a full view
      }
      : view
  }
}

flightSchema.post('find', (result, next) => {
  const config = new ConfigurationInstance()
  if (Array.isArray(result) && result.length > 0) {
    result = result.map((item) => {
      if (isCost(item.checkOutTime)) {
        item.cost += (item.cost * config.keys[PERCENTAGE_INCREMENT_SCHEDULE]) / 100
      }
      return item
    })
  }

  next()
})

const model = mongoose.model('Flight', flightSchema)

export const schema = model.schema
export default model
