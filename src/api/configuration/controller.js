import { success, notFound } from '../../services/response/'
import { Configuration } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Configuration.create(body)
    .then((configuration) => configuration.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Configuration.find(query, select, cursor)
    .then((configurations) => configurations.map((configuration) => configuration.view()))
    .then(success(res))
    .catch(next)

export const showByKey = ({ params }, res, next) =>
  Configuration.findOne({key: params.key})
    .then(notFound(res))
    .then((configuration) => configuration ? configuration.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Configuration.findById(params.id)
    .then(notFound(res))
    .then((configuration) => configuration ? Object.assign(configuration, body).save() : null)
    .then((configuration) => configuration ? configuration.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Configuration.findById(params.id)
    .then(notFound(res))
    .then((configuration) => configuration ? configuration.remove() : null)
    .then(success(res, 204))
    .catch(next)
