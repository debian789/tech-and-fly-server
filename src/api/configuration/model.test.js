import Configuration from './model'

let configuration

beforeEach(async () => {
  configuration = await Configuration.create({ key: 'test', value: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = configuration.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(configuration.id)
    expect(view.key).toBe(configuration.key)
    expect(view.value).toBe(configuration.value)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = configuration.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(configuration.id)
    expect(view.key).toBe(configuration.key)
    expect(view.value).toBe(configuration.value)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
