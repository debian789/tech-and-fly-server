import {ConfigurationInstance, isCost} from './utils'

test('Implement ConfigurationInstance success with array', () => {
  const config = new ConfigurationInstance()
  const defaultData = [{key: 'PERCENTAGE_INCREMENT_SCHEDULE', value: '2345'}]
  config.keys = defaultData
  expect(config.keys['PERCENTAGE_INCREMENT_SCHEDULE']).toEqual('2345')
  const config2 = new ConfigurationInstance()
  expect(config2.keys['PERCENTAGE_INCREMENT_SCHEDULE']).toEqual('2345')
})

test('Implement ConfigurationInstance success with object', () => {
  const config = new ConfigurationInstance()
  const defaultData = {key: 'PERCENTAGE_INCREMENT_SCHEDULE', value: '2345'}
  config.keys = defaultData
  expect(config.keys['PERCENTAGE_INCREMENT_SCHEDULE']).toEqual('2345')
  const config2 = new ConfigurationInstance()
  expect(config2.keys['PERCENTAGE_INCREMENT_SCHEDULE']).toEqual('2345')
})

test('Implement ConfigurationInstance fail', () => {
  const config = new ConfigurationInstance()
  const defaultData = [{valor: 'PERCENTAGE_INCREMENT_SCHEDULE', resultado: '2345'}]
  config.keys = defaultData
  expect(config.keys['PERCENTAGE_INCREMENT_SCHEDULE']).toEqual(undefined)
  expect(config.keys).toEqual({})
})

test('apply cost by date', () => {
  let cost = isCost(new Date(2012, 1, 29, 11, 45))
  expect(cost).toEqual(true)
  cost = isCost(new Date(2012, 1, 29, 16, 45))
  expect(cost).toEqual(false)
  cost = isCost(new Date(2018, 6, 21, 10, 45))
  expect(cost).toEqual(true)
  cost = isCost(new Date(2018, 1, 11, 8, 45))
  expect(cost).toEqual(true)
  cost = isCost('lunes')
  expect(cost).toEqual(undefined)
})
