export const PERCENTAGE_INCREMENT_SCHEDULE = 'PERCENTAGE_INCREMENT_SCHEDULE'
export const ERROR = {
  RESERVATION: {
    SAVE_RESERVATION: {
      message: 'Only one reservation per day is allowed',
      code: 1
    }
  }
}
