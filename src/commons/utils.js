import {isSaturday, isSunday, isDate, getHours} from 'date-fns'
let instance = null

export class ConfigurationInstance {
  constructor () {
    if (!instance) {
      instance = this
    }

    return instance
  }

  get keys () {
    return this._type
  }

  set keys (value) {
    let result = {}

    if (Array.isArray(value) && value.length > 0) {
      value.map(item => {
        if (item.key && item.value) {
          result[item.key] = item.value
        }
      })
      this._type = result
    } else {
      if (value.key && value.value) {
        result[value.key] = value.value
      }

      this._type = result
    }
  }
}

export const isCost = (inputDate) => {
  if (isDate(inputDate)) {
    if (isSaturday(inputDate) || isSunday(inputDate)) {
      return true
    } else if (getHours(inputDate) <= 12) {
      return true
    } else {
      return false
    }
  } else {
    return undefined
  }
}
