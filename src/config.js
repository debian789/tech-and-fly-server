/* eslint-disable no-unused-vars */
import path from 'path'
import merge from 'lodash/merge'
import isEmpty from 'lodash/isEmpty'
import configuration from './api/configuration/model'
import {PERCENTAGE_INCREMENT_SCHEDULE} from './commons/constants'
import {ConfigurationInstance} from './commons/utils'

const loadDefaultConfiguration = () => {
  configuration
    .findOne({key: PERCENTAGE_INCREMENT_SCHEDULE})
    .then(result => {
      if (isEmpty(result)) {
        configuration
          .create({key: PERCENTAGE_INCREMENT_SCHEDULE, value: requireProcessEnv('PERCENTAGE_INCREMENT_SCHEDULE')})
          .then((resultSave) => {
            let configurationInstance = new ConfigurationInstance()
            configurationInstance.keys = resultSave

            console.log(`Create: ${PERCENTAGE_INCREMENT_SCHEDULE}`)
          })
          .catch(() => {
            console.log(`No create: ${PERCENTAGE_INCREMENT_SCHEDULE}`)
          })
      } else {
        let configurationInstance = new ConfigurationInstance()
        configurationInstance.keys = result
      }
    })
    .catch(err => console.log(err))
}

// load default value config
loadDefaultConfiguration()

/* istanbul ignore next */
const requireProcessEnv = (name) => {
  if (!process.env[name]) {
    throw new Error('You must set the ' + name + ' environment variable')
  }
  return process.env[name]
}

/* istanbul ignore next */
if (process.env.NODE_ENV !== 'production') {
  const dotenv = require('dotenv-safe')
  dotenv.load({
    path: path.join(__dirname, '../.env'),
    sample: path.join(__dirname, '../.env.example')
  })
}

const config = {
  all: {
    env: process.env.NODE_ENV || 'development',
    root: path.join(__dirname, '..'),
    port: process.env.PORT || 9000,
    ip: process.env.IP || '0.0.0.0',
    apiRoot: process.env.API_ROOT || '',
    masterKey: requireProcessEnv('MASTER_KEY'),
    mongo: {
      options: {
        db: {
          safe: true
        }
      }
    }
  },
  test: {},
  development: {
    mongo: {
      uri: 'mongodb://localhost/tech-and-fly-dev',
      options: {
        debug: true
      }
    }
  },
  production: {
    ip: process.env.IP || undefined,
    port: process.env.PORT || 8080,
    mongo: {
      uri: process.env.MONGODB_URI || 'mongodb://localhost/tech-and-fly'
    }
  }
}

module.exports = merge(config.all, config[config.all.env])
export default module.exports
